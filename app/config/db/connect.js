const mongoose = require('mongoose');

//URI: Mongo cloud
mongoose.Promise - global.Promise;
mongoose.connect('mongodb+srv://scot39:pegasus@node-cluster-api.ptd40.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',  { 
  useNewUrlParser: true,
  useUnifiedTopology: true 
});

/*
//Local
mongoose.Promise - global.Promise;
mongoose.connect('mongodb://localhost:27017/node-crud-api',  { 
  useNewUrlParser: true,
  useUnifiedTopology: true 
});
*/
module.exports = mongoose