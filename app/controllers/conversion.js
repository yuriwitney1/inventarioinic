const axios = require('axios');
const { response } = require('express');

//Conversor real dolar
const RealDolar = async () => {
    try{
      const response = await axios.get('https://economia.awesomeapi.com.br/all/USD-BRL');
      return response;
    }catch(err){
      logger.error(err);
    }
  };
  
  module.exports = {
    RealDolar,
  };
  
  