const express = require('express');
const Produto = require('../models/produto');
const conversor = require('../controllers/conversion');
const router = express.Router();
const valorEmDolar = conversor.RealDolar()

//Rotas que terminarem com /produtos  (GET ALL e POST)
router.route('/produtos')

    /** Método 1: Criar produto (Acessar em: http://localhost:8000/api/produtos)*/
  .post((req, res) => {
    let produto = new Produto();

    //Aqui vamos setar os campos do produto (via request):
    produto.nome = req.body.nome;
    produto.preco = req.body.preco;
    produto.descricao = req.body.descricao;
    produto.save( err => {

      if(err) {
        res.status(400).send('Erro ao tentar salvar o produto....:' + err);
      } else {
        res.status(201).json(produto);
        
      }

    });
  })
  /** Método 2: Selecionar todos os produtos (Acessar em: http://localhost:8000/api/produtos)*/
  .get((req, res) => {
    Produto.find((err, produtos) => {
      
      if(err) {
        res.status(400).send('Erro ao tentar selecionar todos os produtos: ' + err);
      } else {
        res.status(200).json(produtos);
      }
        
    });

  })
    // Rotas terminando em '/produtos/:produtos_id' (Servirá para GET, PUT & DELETE):
    router.route('/produtos/:produto_id')

    /** Método 3: Selecionar por ID (Acessar em: GET http://localhost:8000/api/produtos/:produto_id)*/
    
    .get((req, res) => {
        Produto.findById(req.params.produto_id, (err, produto) => {
          if(err) {
            res.send('ID do produto não encontrado: ' + err);
          } else {
            valorEmDolar.then(dolar => {
              //console.log(valor.data.USD.high)
              //console.log(produto.preco * valor.data.USD.high);
              produto.preco = produto.preco * dolar.data.USD.high
              });
            produto.save((err) => {
              if(err) {
                res.status(400).send('Erro ao atualizar o produto' + err);
              } else {
                res.status(200).json(produto);
              }
          })
            
          }

        
        
      })
    })
      
  /** Método 4: Atualizar por ID (Acessar em: PUT http://localhost:8000/api/produtos/:produto_id)*/
  .put((req,res) => {

    //primeiro: procurar ID do produto
    Produto.findById(req.params.produto_id, (err, produto) => {
      if(err) {
        res.status(400).send('ID do produto não encontrado: '+ err);
      } else {
    //segundo: atualizar os campos do produto
        produto.nome = req.body.nome;
        produto.preco = req.body.preco;
        produto.descricao = req.body.descricao;

    //terceiro: salvar mudanças
        produto.save((err) => {
          if(err) {
            res.status(400).send('Erro ao atualizar o produto' + err);
          } else {
            res.status(200).json(produto);
          }
          
        });
      }
    });
  })
  /** Método 5: Excluir por ID (Acessar em: DELETE http://localhost:8000/api/produtos/:produto_id)*/
  .delete((req, res) => {
    Produto.deleteOne({
      _id: req.params.produto_id
      }, (err, resultado) => {
        if(err) {
          res.status(400).send('ID do produto não encontrado' + err);
        } else {
          res.status(200).json(resultado);
        }
      }
    );
  });
module.exports = router