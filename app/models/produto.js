/*
 * Arquivo: produto.js
 * Autor: Yuri Barros
 * Descrição: Arquivo responsável por tratar o modelo da classe "Produto"
 * Data: 10/04/2021
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/** 
 * Produto:
 * 
 * -> Id: int
 * -> Nome: string
 * -> Preço: number
 * -> Descrição: string
 * 
*/

var ProdutoSchema = new Schema({
    nome: String,
    preco: Number,
    descricao: String
}); 

module.exports = mongoose.model('Produto', ProdutoSchema);