//Rotas da API
//********************************************************************

const express = require('express');
const { model } = require('mongoose');
//Criação das rotas via express:
const productController = require('../controllers/produto');//express.Router();

productController.use((req, res, next) => {
    console.log('Algo está acontecendo aqui...');
    next();
});

//Rota de teste:
productController.get('/', (req, res) => {
    res.json({ message: 'Beleza! Bem vindo a nossa loja xyz'})
});

//Rotas que terminarem com /produtos  (GET ALL e POST)
productController.route('/produtos')

  
//API's:
/************************************************************** */
module.exports = productController