/*
 * Arquivo: server.js
 * Descrição: Levantar serviço do node api
 * Autor: Yuri Barros
 * 
 */

// Configurar o setup da app:

//Chamada dos pacotes:
const express = require('express');
const app = express();
const router = require('./app/routes/rota');
const mongoose = require('./app/config/db/connect');

//Configuração da variável app para usar o express():
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//Definindo porta para execução da api:
const port = process.env.port || 8000;

//Definindo padrão prefixado com '/api':
app.use('/api', router);

const axios = require('axios');
/*
//Conversor real dolar
const conversor = require('./controllers/conversion');
const getNowPlayingMovies = async () => {
  const response = await axios.get('https://economia.awesomeapi.com.br/all/USD-BRL');
  console.log(response.data.USD.high * 2);

};
getNowPlayingMovies()
*/
//Iniciando app:
app.listen(port);
//console.log("Iniciando app na porta " + port);
